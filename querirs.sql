-- 1. Find the Most Popular Destination Based on Number of Booked Flights
SELECT c.Name AS CityName, COUNT(f.FlightCode) AS NumberOfFlights
FROM flights f
JOIN airports a ON f.ArrivalAirportCode = a.AirportCode
JOIN cities c ON a.CityCode = c.CityCode
GROUP BY c.CityCode
ORDER BY NumberOfFlights DESC
LIMIT 1;
-- 2. List Clients and Their Total Spend on Trips
SELECT cl.ClientName, SUM(vpp.BasePrice) AS TotalSpend
FROM clients cl
JOIN orders o ON cl.ClientCode = o.ClientCode
JOIN vacation_package_in_order vpio ON o.OrderCode = vpio.OrderCode
JOIN vacation_package_plan vpp ON vpio.PackageCode = vpp.PackageCode
GROUP BY cl.ClientCode
ORDER BY TotalSpend DESC;
-- 3. Detailed for a Specific Client's Booking
SELECT  cl.ClientName, f.DateOfFlying, dep.Name AS DepartureAirport, arr.Name AS ArrivalAirport, h.Name AS HotelName, vpp.DateOfTrip, vpp.Duration, vpio.TotalPassengers
FROM clients cl
JOIN orders o ON cl.ClientCode = o.ClientCode
JOIN vacation_package_in_order vpio ON o.OrderCode = vpio.OrderCode
JOIN vacation_package_plan vpp ON vpio.PackageCode = vpp.PackageCode
JOIN hotels h ON vpp.HotelCode = h.HotelCode
JOIN flights_in_vacation_plan fivp ON vpp.PackageCode = fivp.PackageCode
JOIN flights f ON fivp.FlightCode = f.FlightCode
JOIN airports dep ON f.DepartureAirportCode = dep.AirportCode
JOIN airports arr ON f.ArrivalAirportCode = arr.AirportCode
WHERE cl.ClientCode = 9
ORDER BY f.DateOfFlying, vpp.DateOfTrip;
-- Average Flight Prices by Destination
SELECT 
    c.Name AS Destination, 
    AVG(fc.PriceForAdult) AS AvgAdultPrice, 
    AVG(fc.PriceForChild) AS AvgChildPrice
FROM flights f
JOIN airports a ON f.ArrivalAirportCode = a.AirportCode
JOIN cities c ON a.CityCode = c.CityCode
JOIN flight_companies fc ON f.CompanyCode = fc.CompanyCode
GROUP BY c.CityCode
ORDER BY AvgAdultPrice DESC, AvgChildPrice DESC;
-- Hotels with Most Bookings
SELECT 
    h.Name AS HotelName, 
    c.Name AS City, 
    COUNT(vpio.PackageCode) AS NumberOfBookings
FROM vacation_package_in_order vpio
JOIN vacation_package_plan vpp ON vpio.PackageCode = vpp.PackageCode
JOIN hotels h ON vpp.HotelCode = h.HotelCode
JOIN cities c ON h.CityCode = c.CityCode
GROUP BY h.HotelCode
ORDER BY NumberOfBookings DESC;
-- Flight and Hotel Package Popularity
SELECT 
    vpp.PackageCode,
    h.Name AS HotelName,
    SUM(vpio.TotalPassengers) AS TotalPassengers
FROM vacation_package_in_order vpio
JOIN vacation_package_plan vpp ON vpio.PackageCode = vpp.PackageCode
JOIN hotels h ON vpp.HotelCode = h.HotelCode
GROUP BY vpp.PackageCode
ORDER BY TotalPassengers DESC;
-- airport travels
SELECT 
    dep.Name AS DepartureAirport, 
    arr.Name AS ArrivalAirport, 
    COUNT(f.FlightCode) AS NumberOfTrips
FROM flights f
JOIN airports dep ON f.DepartureAirportCode = dep.AirportCode
JOIN airports arr ON f.ArrivalAirportCode = arr.AirportCode
JOIN flights_in_vacation_plan fivp ON f.FlightCode = fivp.FlightCode
JOIN vacation_package_in_order vpio ON fivp.PackageCode = vpio.PackageCode
JOIN orders o ON vpio.OrderCode = o.OrderCode
GROUP BY dep.AirportCode, arr.AirportCode
ORDER BY NumberOfTrips DESC;

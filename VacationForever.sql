-- MySQL dump 10.13  Distrib 8.0.36, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: vacationmanager
-- ------------------------------------------------------
-- Server version	8.1.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `airports`
--

DROP TABLE IF EXISTS `airports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `airports` (
  `AirportCode` varchar(10) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `CityCode` int DEFAULT NULL,
  PRIMARY KEY (`AirportCode`),
  KEY `CityCode` (`CityCode`),
  CONSTRAINT `airports_ibfk_1` FOREIGN KEY (`CityCode`) REFERENCES `cities` (`CityCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airports`
--

LOCK TABLES `airports` WRITE;
/*!40000 ALTER TABLE `airports` DISABLE KEYS */;
INSERT INTO `airports` VALUES ('CDG','Charles de Gaulle',1),('FCO','Leonardo da Vinci International Airport',4),('HND','Haneda Airport',3),('JFK','John F. Kennedy International Airport',2),('SYD','Sydney Airport',6),('YYZ','Toronto Pearson International Airport',5);
/*!40000 ALTER TABLE `airports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cities` (
  `CityCode` int NOT NULL,
  `Name` varchar(255) NOT NULL,
  `CountryCode` int DEFAULT NULL,
  PRIMARY KEY (`CityCode`),
  KEY `CountryCode` (`CountryCode`),
  CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`CountryCode`) REFERENCES `countries` (`CountryCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Paris',1),(2,'New York',2),(3,'Tokyo',3),(4,'Rome',4),(5,'Toronto',5),(6,'Sydney',6);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients` (
  `ClientCode` int NOT NULL AUTO_INCREMENT,
  `ClientName` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `PhoneNumber` varchar(20) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `CityCode` int DEFAULT NULL,
  PRIMARY KEY (`ClientCode`),
  UNIQUE KEY `Email` (`Email`),
  KEY `CityCode` (`CityCode`),
  CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`CityCode`) REFERENCES `cities` (`CityCode`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (9,'John Doe','john.doe@example.com','1234567890','123 Main St, Paris',1),(10,'Jane Smith','jane.smith@example.com','0987654321','456 Park Ave, New York',2),(11,'Marco Rossi','marco.rossi@example.com','555-0102','234 Via Roma, Rome',4),(12,'Emily White','emily.white@example.com','555-0103','123 Maple Street, Toronto',5),(13,'Lucas Brown','lucas.brown@example.com','555-0104','456 Harbour Road, Sydney',6),(14,'Jane Doe','jane.doe@example.com','123-456-7890','456 Elm Street',2);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `countries` (
  `CountryCode` int NOT NULL,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`CountryCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'France'),(2,'United States'),(3,'Japan'),(4,'Italy'),(5,'Canada'),(6,'Australia');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight_companies`
--

DROP TABLE IF EXISTS `flight_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flight_companies` (
  `CompanyCode` int NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(255) NOT NULL,
  `PriceForAdult` decimal(10,2) NOT NULL,
  `PriceForChild` decimal(10,2) NOT NULL,
  `PriceForLuggage` decimal(10,2) NOT NULL,
  `LuggageAmountKG` decimal(10,2) NOT NULL,
  `Capacity` int NOT NULL,
  PRIMARY KEY (`CompanyCode`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight_companies`
--

LOCK TABLES `flight_companies` WRITE;
/*!40000 ALTER TABLE `flight_companies` DISABLE KEYS */;
INSERT INTO `flight_companies` VALUES (16,'Air France',500.00,250.00,20.00,50.00,240),(17,'Delta Airlines',450.00,225.00,25.00,50.00,320),(18,'Japan Airlines',550.00,275.00,30.00,50.00,112),(19,'Qantas Airways',600.00,300.00,25.00,50.00,230),(20,'Air Canada',550.00,275.00,20.00,50.00,220),(21,'Alitalia',500.00,250.00,30.00,50.00,240);
/*!40000 ALTER TABLE `flight_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flights`
--

DROP TABLE IF EXISTS `flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flights` (
  `FlightCode` int NOT NULL,
  `DateOfFlying` date NOT NULL,
  `DepartureAirportCode` varchar(10) DEFAULT NULL,
  `ArrivalAirportCode` varchar(10) DEFAULT NULL,
  `CompanyCode` int DEFAULT NULL,
  `BookedPassengers` int DEFAULT '0',
  PRIMARY KEY (`FlightCode`),
  KEY `DepartureAirportCode` (`DepartureAirportCode`),
  KEY `ArrivalAirportCode` (`ArrivalAirportCode`),
  KEY `fk_flight_company` (`CompanyCode`),
  CONSTRAINT `fk_flight_company` FOREIGN KEY (`CompanyCode`) REFERENCES `flight_companies` (`CompanyCode`),
  CONSTRAINT `flights_ibfk_1` FOREIGN KEY (`DepartureAirportCode`) REFERENCES `airports` (`AirportCode`),
  CONSTRAINT `flights_ibfk_2` FOREIGN KEY (`ArrivalAirportCode`) REFERENCES `airports` (`AirportCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flights`
--

LOCK TABLES `flights` WRITE;
/*!40000 ALTER TABLE `flights` DISABLE KEYS */;
INSERT INTO `flights` VALUES (1,'2024-03-15','CDG','JFK',16,230),(2,'2024-03-16','JFK','HND',17,38),(3,'2024-03-17','HND','CDG',18,57),(4,'2024-04-15','FCO','YYZ',19,30),(5,'2024-04-18','YYZ','SYD',20,91),(6,'2024-04-20','SYD','FCO',21,102);
/*!40000 ALTER TABLE `flights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flights_in_vacation_plan`
--

DROP TABLE IF EXISTS `flights_in_vacation_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flights_in_vacation_plan` (
  `FlightCode` int NOT NULL,
  `PackageCode` int NOT NULL,
  PRIMARY KEY (`FlightCode`,`PackageCode`),
  KEY `PackageCode` (`PackageCode`),
  CONSTRAINT `flights_in_vacation_plan_ibfk_1` FOREIGN KEY (`FlightCode`) REFERENCES `flights` (`FlightCode`),
  CONSTRAINT `flights_in_vacation_plan_ibfk_2` FOREIGN KEY (`PackageCode`) REFERENCES `vacation_package_plan` (`PackageCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flights_in_vacation_plan`
--

LOCK TABLES `flights_in_vacation_plan` WRITE;
/*!40000 ALTER TABLE `flights_in_vacation_plan` DISABLE KEYS */;
INSERT INTO `flights_in_vacation_plan` VALUES (1,9),(4,9),(2,10),(5,10),(3,11),(6,11);
/*!40000 ALTER TABLE `flights_in_vacation_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotels`
--

DROP TABLE IF EXISTS `hotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotels` (
  `HotelCode` int NOT NULL,
  `Name` varchar(255) NOT NULL,
  `CityCode` int DEFAULT NULL,
  `Rating` int DEFAULT NULL,
  PRIMARY KEY (`HotelCode`),
  KEY `CityCode` (`CityCode`),
  CONSTRAINT `hotels_ibfk_1` FOREIGN KEY (`CityCode`) REFERENCES `cities` (`CityCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotels`
--

LOCK TABLES `hotels` WRITE;
/*!40000 ALTER TABLE `hotels` DISABLE KEYS */;
INSERT INTO `hotels` VALUES (1,'The Ritz Paris',1,5),(2,'The Plaza Hotel',2,5),(3,'The Imperial Hotel',3,5),(4,'Hotel Canada',5,4),(5,'Sydney Harbour Hotel',6,5),(6,'Rome Ancient Hotel',4,3);
/*!40000 ALTER TABLE `hotels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `OrderCode` int NOT NULL AUTO_INCREMENT,
  `ClientCode` int DEFAULT NULL,
  `DateOfOrder` date NOT NULL,
  `AmountOfChildren` int NOT NULL,
  `AmountOfAdults` int NOT NULL,
  PRIMARY KEY (`OrderCode`),
  KEY `ClientCode` (`ClientCode`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`ClientCode`) REFERENCES `clients` (`ClientCode`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (34,9,'2024-02-20',2,2),(35,10,'2024-02-25',1,3),(36,9,'2024-03-22',0,2),(37,10,'2024-03-25',1,1),(38,11,'2024-04-05',2,2),(39,12,'2024-04-10',0,1),(40,13,'2024-04-15',1,2),(41,9,'2024-03-22',2,1),(45,9,'2024-03-12',2,5),(46,11,'2024-03-12',2,5);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passengers`
--

DROP TABLE IF EXISTS `passengers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `passengers` (
  `PassportNumber` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `BirthDate` date NOT NULL,
  `CountryCode` int NOT NULL,
  PRIMARY KEY (`PassportNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passengers`
--

LOCK TABLES `passengers` WRITE;
/*!40000 ALTER TABLE `passengers` DISABLE KEYS */;
INSERT INTO `passengers` VALUES ('P11223344','Alice','Smith','1990-11-12',3),('P12345678','John','Doe','1980-05-01',2),('P23456789','Lisa','Green','1995-08-15',5),('P34567890','Michael','Brown','1988-02-27',4),('P45678901','Sophia','Wilson','1992-12-05',6),('P56789012','Noah','Brown','1988-03-14',5),('P67890123','Emma','Wilson','1990-12-01',5),('P78901234','Mason','Taylor','1993-09-17',6),('P87654321','Jane','Doe','1985-07-03',1),('P89012345','Isabella','Anderson','1996-11-23',6);
/*!40000 ALTER TABLE `passengers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passengers_in_order`
--

DROP TABLE IF EXISTS `passengers_in_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `passengers_in_order` (
  `OrderCode` int NOT NULL,
  `PassportNumber` varchar(255) NOT NULL,
  PRIMARY KEY (`OrderCode`,`PassportNumber`),
  KEY `PassportNumber` (`PassportNumber`),
  CONSTRAINT `passengers_in_order_ibfk_1` FOREIGN KEY (`OrderCode`) REFERENCES `orders` (`OrderCode`),
  CONSTRAINT `passengers_in_order_ibfk_2` FOREIGN KEY (`PassportNumber`) REFERENCES `passengers` (`PassportNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passengers_in_order`
--

LOCK TABLES `passengers_in_order` WRITE;
/*!40000 ALTER TABLE `passengers_in_order` DISABLE KEYS */;
INSERT INTO `passengers_in_order` VALUES (35,'P11223344'),(34,'P12345678'),(36,'P45678901'),(36,'P56789012'),(37,'P67890123'),(38,'P78901234'),(34,'P87654321'),(38,'P89012345');
/*!40000 ALTER TABLE `passengers_in_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_type_in_hotel`
--

DROP TABLE IF EXISTS `room_type_in_hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_type_in_hotel` (
  `HotelCode` int NOT NULL,
  `TypeCode` int NOT NULL,
  `PriceForOneNightAsACouple` decimal(10,2) DEFAULT NULL,
  `PriceForOneAdult` decimal(10,2) DEFAULT NULL,
  `PriceForAnotherChild` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`HotelCode`,`TypeCode`),
  KEY `TypeCode` (`TypeCode`),
  CONSTRAINT `room_type_in_hotel_ibfk_1` FOREIGN KEY (`HotelCode`) REFERENCES `hotels` (`HotelCode`),
  CONSTRAINT `room_type_in_hotel_ibfk_2` FOREIGN KEY (`TypeCode`) REFERENCES `room_types` (`TypeCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_type_in_hotel`
--

LOCK TABLES `room_type_in_hotel` WRITE;
/*!40000 ALTER TABLE `room_type_in_hotel` DISABLE KEYS */;
INSERT INTO `room_type_in_hotel` VALUES (1,3,200.00,180.00,90.00),(1,4,500.00,450.00,225.00),(1,10,800.00,700.00,350.00),(2,5,250.00,225.00,112.50),(3,6,300.00,270.00,135.00),(4,7,250.00,200.00,100.00),(5,8,300.00,250.00,125.00),(6,9,450.00,400.00,200.00);
/*!40000 ALTER TABLE `room_type_in_hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_types`
--

DROP TABLE IF EXISTS `room_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_types` (
  `TypeCode` int NOT NULL AUTO_INCREMENT,
  `BedsAmount` int NOT NULL,
  `Description` text NOT NULL,
  PRIMARY KEY (`TypeCode`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_types`
--

LOCK TABLES `room_types` WRITE;
/*!40000 ALTER TABLE `room_types` DISABLE KEYS */;
INSERT INTO `room_types` VALUES (3,1,'Single - One single bed'),(4,2,'Double - One double bed'),(5,3,'Twin - Two single beds'),(6,4,'Suite - Multiple rooms with a double bed'),(7,1,'Deluxe Single - One king size bed with deluxe amenities'),(8,2,'Deluxe Double - Two queen size beds with deluxe amenities'),(9,3,'Executive Suite - Includes multiple rooms with executive amenities'),(10,4,'Presidential Suite - Premier suite with luxurious amenities');
/*!40000 ALTER TABLE `room_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacation_package_in_order`
--

DROP TABLE IF EXISTS `vacation_package_in_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacation_package_in_order` (
  `OrderCode` int NOT NULL,
  `PackageCode` int NOT NULL,
  `TotalPassengers` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`OrderCode`,`PackageCode`),
  KEY `PackageCode` (`PackageCode`),
  CONSTRAINT `vacation_package_in_order_ibfk_1` FOREIGN KEY (`OrderCode`) REFERENCES `orders` (`OrderCode`),
  CONSTRAINT `vacation_package_in_order_ibfk_2` FOREIGN KEY (`PackageCode`) REFERENCES `vacation_package_plan` (`PackageCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacation_package_in_order`
--

LOCK TABLES `vacation_package_in_order` WRITE;
/*!40000 ALTER TABLE `vacation_package_in_order` DISABLE KEYS */;
INSERT INTO `vacation_package_in_order` VALUES (35,10,4),(36,11,2),(37,12,2),(38,13,4),(45,14,7),(46,14,7);
/*!40000 ALTER TABLE `vacation_package_in_order` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `vacation_package_in_order_BEFORE_INSERT` BEFORE INSERT ON `vacation_package_in_order` FOR EACH ROW BEGIN
 DECLARE order_id INT;
	  
	  -- Get the OrderCode from the inserted row
	  SET order_id = NEW.OrderCode;
	  
	  -- Update the TotalPassengers in the same inserted row using NEW
	  SET NEW.TotalPassengers = 
		(SELECT SUM(AmountOfChildren + AmountOfAdults) FROM orders WHERE OrderCode = order_id);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `vacation_package_plan`
--

DROP TABLE IF EXISTS `vacation_package_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacation_package_plan` (
  `PackageCode` int NOT NULL AUTO_INCREMENT,
  `DateOfTrip` date NOT NULL,
  `HotelCode` int DEFAULT NULL,
  `BasePrice` decimal(10,2) DEFAULT NULL,
  `Duration` int NOT NULL,
  PRIMARY KEY (`PackageCode`),
  KEY `HotelCode` (`HotelCode`),
  CONSTRAINT `vacation_package_plan_ibfk_1` FOREIGN KEY (`HotelCode`) REFERENCES `hotels` (`HotelCode`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacation_package_plan`
--

LOCK TABLES `vacation_package_plan` WRITE;
/*!40000 ALTER TABLE `vacation_package_plan` DISABLE KEYS */;
INSERT INTO `vacation_package_plan` VALUES (9,'2024-03-15',1,3000.00,4),(10,'2024-03-20',2,3500.00,5),(11,'2024-03-15',1,3000.00,7),(12,'2024-03-20',2,3500.00,5),(13,'2024-05-01',4,2000.00,7),(14,'2024-06-01',5,2500.00,10),(15,'2024-07-01',6,1500.00,5);
/*!40000 ALTER TABLE `vacation_package_plan` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `vacation_package_plan_BEFORE_INSERT` BEFORE INSERT ON `vacation_package_plan` FOR EACH ROW BEGIN
  DECLARE done INT DEFAULT FALSE;
    DECLARE ov_order INT;
    DECLARE orders_cursor CURSOR FOR 
        SELECT o.OrderCode
        FROM orders o
        JOIN vacation_package_in_order vpio ON o.OrderCode = vpio.OrderCode
        JOIN vacation_package_plan vpp ON vpio.PackageCode = vpp.PackageCode
        WHERE (
            (NEW.DateOfTrip BETWEEN vpp.DateOfTrip AND DATE_ADD(vpp.DateOfTrip, INTERVAL vpp.Duration DAY))
            OR (DATE_ADD(NEW.DateOfTrip, INTERVAL NEW.Duration DAY) BETWEEN vpp.DateOfTrip AND DATE_ADD(vpp.DateOfTrip, INTERVAL vpp.Duration DAY))
        );
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    -- Open the cursor to find overlapping orders
    OPEN orders_cursor;
    
    read_loop: LOOP
        FETCH orders_cursor INTO ov_order;
        IF done THEN
            LEAVE read_loop;
        END IF;
        
        -- Deleting related records from 'vacation_package_in_order' and 'orders'
        DELETE FROM vacation_package_in_order WHERE OrderCode = ov_order;
        DELETE FROM orders WHERE OrderCode = ov_order;
    END LOOP;
    
    CLOSE orders_cursor;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'vacationmanager'
--

--
-- Dumping routines for database 'vacationmanager'
--
/*!50003 DROP FUNCTION IF EXISTS `AvgPackagePriceForCountry` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `AvgPackagePriceForCountry`(countryName VARCHAR(255)) RETURNS decimal(10,2)
    DETERMINISTIC
BEGIN
	DECLARE averagePrice DECIMAL(10,2);
    SELECT AVG(vpp.BasePrice) as 'avrage per Country' INTO averagePrice
    FROM vacation_package_plan vpp
    JOIN hotels h ON vpp.HotelCode = h.HotelCode
    JOIN cities c ON h.CityCode = c.CityCode
    JOIN countries co ON c.CountryCode = co.CountryCode
    WHERE co.Name = countryName;
    RETURN averagePrice;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `CalculateDiscountedPriceWithSeason` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `CalculateDiscountedPriceWithSeason`(packageCode INT, discountRate DECIMAL(5,2)) RETURNS decimal(10,2)
    DETERMINISTIC
BEGIN
    DECLARE originalPrice DECIMAL(10,2);
    DECLARE seasonalMultiplier DECIMAL(3,2);
    DECLARE monthOfTrip INT;
    DECLARE discountedPrice DECIMAL(10,2);

	SELECT BasePrice, MONTH(DateOfTrip) INTO originalPrice, monthOfTrip 
    FROM vacation_package_plan 
    WHERE PackageCode = packageCode
    ORDER BY DateOfTrip DESC LIMIT 1;
    CASE
        WHEN monthOfTrip IN (12, 1, 2) THEN SET seasonalMultiplier = 1.20; -- Winter
        WHEN monthOfTrip IN (6, 7, 8) THEN SET seasonalMultiplier = 1.15; -- Summer
        ELSE SET seasonalMultiplier = 1.00; -- Spring/Fall
    END CASE;

    SET discountedPrice = originalPrice - (originalPrice * discountRate / 100);
    RETURN discountedPrice * seasonalMultiplier;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ClientsTotalSpentByCategory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `ClientsTotalSpentByCategory`(clientCode INT) RETURNS varchar(255) CHARSET utf8mb4
    READS SQL DATA
BEGIN
    DECLARE totalSpentFlights DECIMAL(10,2);
    DECLARE totalSpentHotels DECIMAL(10,2);
    DECLARE result VARCHAR(255);

    -- Calculate total spent on flights
    SELECT SUM(fc.PriceForAdult * o.AmountOfAdults + fc.PriceForChild* o.AmountOfChildren) INTO totalSpentFlights
    FROM orders o
    join vacation_package_in_order vpio on vpio.OrderCode = o.OrderCode
    join vacation_package_plan vpp on vpp.PackageCode = vpio.PackageCode
    JOIN flights_in_vacation_plan fivp ON fivp.PackageCode = vpp.PackageCode
    join flights fl on fl.FlightCode = fivp.FlightCode
    JOIN flight_companies fc ON fl.CompanyCode = fc.CompanyCode
    WHERE o.ClientCode = clientCode;

    -- Calculate total spent on hotels through vacation packages
    SELECT SUM(vpp.BasePrice) INTO totalSpentHotels
    FROM orders o
    JOIN vacation_package_in_order vpio ON o.OrderCode = vpio.OrderCode
    JOIN vacation_package_plan vpp ON vpio.PackageCode = vpp.PackageCode
    WHERE o.ClientCode = clientCode;

    SET result = CONCAT('Total Spent - Flights: ', totalSpentFlights, ', Hotels: ', totalSpentHotels);
    RETURN result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `TotalFlightCapacity` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `TotalFlightCapacity`(companyName VARCHAR(255)) RETURNS int
    DETERMINISTIC
BEGIN
    DECLARE totalCapacity INT DEFAULT 0;
    SELECT SUM(fc.Capacity) INTO totalCapacity 
    FROM flights f
    JOIN flight_companies fc ON f.CompanyCode = fc.CompanyCode
    WHERE fc.CompanyName = companyName;
    RETURN totalCapacity;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddNewClient` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddNewClient`(
    IN p_ClientName VARCHAR(255), 
    IN p_Email VARCHAR(255), 
    IN p_PhoneNumber VARCHAR(20), 
    IN p_Address VARCHAR(255), 
    IN p_CityCode INT
)
BEGIN
    INSERT INTO clients (ClientName, Email, PhoneNumber, Address, CityCode)
    VALUES (p_ClientName, p_Email, p_PhoneNumber, p_Address, p_CityCode);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `BookFlightForClient` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `BookFlightForClient`(
    IN p_ClientCode INT,IN p_FlightCode INT,IN p_PackageCode INT,IN p_AmountOfChildren INT,IN p_AmountOfAdults INT)
BEGIN
    DECLARE v_FlightCapacity INT;
    DECLARE v_BookedPassengers INT;
    DECLARE v_TotalNewPassengers INT;
    DECLARE v_AvailableSeats INT;
    START TRANSACTION;
    -- Check if client exists
    IF NOT EXISTS (SELECT * FROM clients WHERE ClientCode = p_ClientCode) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Client does not exist.';
    END IF;
    -- Check if flight exists and get its capacity and booked passengers
    SELECT fc.Capacity, f.BookedPassengers INTO v_FlightCapacity, v_BookedPassengers
    FROM flights f
    JOIN flight_companies fc ON f.CompanyCode = fc.CompanyCode
    WHERE f.FlightCode = p_FlightCode;
    SET v_TotalNewPassengers = p_AmountOfAdults + p_AmountOfChildren;
    SET v_AvailableSeats = v_FlightCapacity - v_BookedPassengers;
    -- Check if there are enough available seats
    IF v_TotalNewPassengers > v_AvailableSeats THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Not enough seats available.';
    END IF;
    -- Insert a new order
    INSERT INTO orders (ClientCode, DateOfOrder, AmountOfChildren, AmountOfAdults)
    VALUES (p_ClientCode, CURDATE(), p_AmountOfChildren, p_AmountOfAdults);
    -- Update booked passengers for the flight
    UPDATE flights SET BookedPassengers = v_BookedPassengers + v_TotalNewPassengers
    WHERE FlightCode = p_FlightCode;
    -- Link the order with the vacation package
    INSERT INTO vacation_package_in_order (OrderCode, PackageCode, TotalPassengers)
    VALUES (LAST_INSERT_ID(), p_PackageCode, v_TotalNewPassengers);
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DeleteExpiredPackages` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `DeleteExpiredPackages`()
BEGIN
    DECLARE v_DeletedRows INT;

    -- Delete expired vacation packages
    DELETE FROM vacation_package_plan
    WHERE DateOfTrip < CURDATE();

    -- Get the number of affected rows
    SET v_DeletedRows = ROW_COUNT();
    SELECT v_DeletedRows AS DeletedPackages;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateFlightPrices` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateFlightPrices`()
BEGIN
    DECLARE v_flightCode INT;
    DECLARE v_demand INT;
    DECLARE finished INTEGER DEFAULT 0;
    -- Cursor to select flight codes and their booking demand
    DECLARE flight_cursor CURSOR FOR 
        SELECT f.FlightCode, COUNT(o.OrderCode) as demand
        FROM flights f
        JOIN flights_in_vacation_plan fivp ON f.FlightCode = fivp.FlightCode
        JOIN orders o ON fivp.PackageCode = o.OrderCode
        GROUP BY f.FlightCode;
    -- Handler for cursor completion
    DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
    -- Start transaction
    START TRANSACTION;
    OPEN flight_cursor;
    update_loop: LOOP
        FETCH flight_cursor INTO v_flightCode, v_demand;
        IF finished = 1 THEN 
            LEAVE update_loop;
        END IF;
        -- Increase prices by 10% for high demand flights
        IF v_demand > 10 THEN
            UPDATE flight_companies fc
            JOIN flights f ON fc.CompanyCode = f.CompanyCode
            SET fc.PriceForAdult = fc.PriceForAdult * 1.1,
                fc.PriceForChild = fc.PriceForChild * 1.1
            WHERE f.FlightCode = v_flightCode;
        END IF;
    END LOOP;
    CLOSE flight_cursor;
    -- Commit the transaction
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-12 12:43:07
